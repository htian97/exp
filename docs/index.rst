project name
======

``project name`` is a package based on `Google Scholar <https://scholar.google.com>`_ for some functionalities.

 - Bullet point 1.  
 - Bullet point 2.

.. toctree::
   :maxdepth: 1

   installation
   feature


License
-------

Package is licensed under the MIT License. 

